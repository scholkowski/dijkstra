//
// Created by maurice on 1/11/23.
//

#include <iostream>
#include <algorithm>
#include <limits>
#include <map>
#include <set>
#include "Graph.h"

Node *Graph::getNode(std::string name) {
    for (auto &node: nodes) {
        if (node->name == name) {
            return node;
        }
    }
    return nullptr;
}

void Graph::addNode(std::string name) {
    for (auto &node: nodes) {
        if (node->name == name) {
            std::cout << "Node already in graph" << std::endl;
            return;
        }
    }

    nodes.push_back(new Node{name});
}

void Graph::addEdge(std::string from, std::string to, unsigned int weight) {
    auto fromNode = Graph::getNode(from);
    auto toNode = Graph::getNode(to);

    if (fromNode == nullptr) {
        std::cout << "Node " << from << " not in graph!" << std::endl;
        return;
    }

    if (toNode == nullptr) {
        std::cout << "Node " << to << " not in graph!" << std::endl;
        return;
    }

    for (auto &edge: fromNode->edges) {
        if (edge.destination == toNode) {
            std::cout << "Edge already exists!" << std::endl;
            return;
        }
    }

    fromNode->edges.push_back({toNode, weight});
    toNode->edges.push_back({fromNode, weight});
}

void Graph::printConnections() {
    for (auto &node: nodes) {
        std::cout << node->name << "->" << std::flush;
        for (auto &edge: node->edges) {
            std::cout << " (" << edge.destination->name << ": " << edge.weight << ")" << std::flush;
        }
        std::cout << std::endl;
    }
}

void Graph::dijkstra(Node *start, Node *destination) {
    // Check if nodes are in graph
    if ((std::find(nodes.begin(), nodes.end(), start)) == nodes.end()) {
        std::cout << "Start node not in graph!" << std::endl;
        return;
    }

    if (((std::find(nodes.begin(), nodes.end(), destination)) == nodes.end()) && destination != nullptr) {
        std::cout << "Destination node not in graph!" << std::endl;
        return;
    }

    // Struct to use in the shortest paths map
    struct Entry {
        unsigned int costs;
        Node *previous;
    };

    // Store shortest paths in map
    std::map<Node *, Entry> shortestPaths;

    // Nodes to be visited
    std::set<Node *> queue;

    // Create Entry in map for each node in Graph. Set weight to infinity (max value of uint) and previous to nullptr
    for (auto &node: nodes) {
        shortestPaths[node] = {std::numeric_limits<unsigned int>::max(), nullptr};
    }

    // Set Entry of start node
    shortestPaths.at(start).costs = 0;
    //shortestPaths.at(start).previous = start;

    // Add start node to queue
    queue.insert(start);

    // Iterate until queue is empty
    while (!queue.empty()) {

        // Get node, with minimal costs saved in the shortest paths map, out of queue
        unsigned int min = std::numeric_limits<unsigned int>::max();
        Node *currentNode = nullptr;
        for (auto &node: queue) {
            if (shortestPaths.at(node).costs < min) {
                min = shortestPaths.at(node).costs;
                currentNode = node;
            }
        }

        // Shouldn't happen - no node found
        if (currentNode == nullptr) {
            std::cout << "Current node is null!" << std::endl;
        }

        // If we have a destination node we can terminate as soon as we reached this node
        if (destination != nullptr && destination == currentNode) {
            std::vector<Node *> reversedPath;
            reversedPath.push_back(destination);

            Node *prev = shortestPaths.at(destination).previous;

            while (prev != nullptr) {
                reversedPath.push_back(prev);
                prev = shortestPaths.at(prev).previous;
            }

            std::cout << "Shortest path from " << start->name << " to " << destination->name << ": " << std::flush;
            for (auto it = reversedPath.rbegin(); it != reversedPath.rend(); ++it) {
                if (it != reversedPath.rbegin()) {
                    std::cout << "->" << std::flush;
                }
                std::cout << (*it)->name << std::flush;
            }
            std::cout << " " << shortestPaths.at(destination).costs << std::endl;
            return;
        }

        // Get costs from start node to current node so far
        unsigned int currentNodeCosts = shortestPaths.at(currentNode).costs;

        // Search all edges in current node
        for (auto &edge: currentNode->edges) {
            // Calculate costs
            unsigned int costs = currentNodeCosts + edge.weight;
            // Only update if calculated costs are less than costs saved in shortestPaths map
            if (costs < shortestPaths.at(edge.destination).costs) {
                // Update shortestPaths map if needed
                shortestPaths.at(edge.destination).costs = costs;
                shortestPaths.at(edge.destination).previous = currentNode;
                // Only insert if entry is updated and if node isn't already in queue
                queue.insert(edge.destination);
            }
        }

        // Remove current node from queue
        queue.erase(currentNode);

    }

    // Print results
    for (auto &entry: shortestPaths) {
        if (entry.second.previous != nullptr) {
            std::cout << "Shortest path to " << entry.first->name << " is " << entry.second.costs << " over node "
                      << entry.second.previous->name << std::endl;
        }
    }

}