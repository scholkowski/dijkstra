//
// Created by maurice on 1/11/23.
//

#ifndef GRAPH_GRAPH_H
#define GRAPH_GRAPH_H

#include <string>
#include <list>

struct Node {
    std::string name;

    struct Edge {
        Node *destination;
        unsigned int weight;
    };

    std::list<Edge> edges;
};

class Graph {
private:
    std::list<Node *> nodes;
public:
    void addNode(std::string name);

    void addEdge(std::string from, std::string to, unsigned int weight);

    void printConnections();

    Node *getNode(std::string name);

    void dijkstra(Node *start, Node *destination = nullptr);
};


#endif //GRAPH_GRAPH_H
