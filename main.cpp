#include <iostream>
#include "Graph.h"

int main() {
    Graph g;

    g.addNode("Frankfurt");
    g.addNode("Mannheim");
    g.addNode("Würzburg");
    g.addNode("Stuttgart");
    g.addNode("Karlsruhe");
    g.addNode("Erfurt");
    g.addNode("Nürnberg");
    g.addNode("Kassel");
    g.addNode("Augsburg");
    g.addNode("München");

    g.addEdge("Frankfurt", "Mannheim", 85);
    g.addEdge("Frankfurt", "Kassel", 173);
    g.addEdge("Augsburg", "München", 84);
    g.addEdge("Erfurt", "Würzburg", 186);
    g.addEdge("Würzburg", "Nürnberg", 103);
    g.addEdge("Frankfurt", "Würzburg", 217);
    g.addEdge("Stuttgart", "Nürnberg", 183);
    g.addEdge("Kassel", "München", 502);
    g.addEdge("München", "Nürnberg", 167);
    g.addEdge("Karlsruhe", "Augsburg", 250);
    g.addEdge("Mannheim", "Karlsruhe", 80);


    std::cout << "\n***********************************************************************************\n" << std::endl;

    g.printConnections();

    std::cout << "\n***********************************************************************************\n" << std::endl;

    g.dijkstra(g.getNode("Frankfurt"), g.getNode("Frankfurt"));
    g.dijkstra(g.getNode("Frankfurt"), g.getNode("Mannheim"));
    g.dijkstra(g.getNode("Frankfurt"), g.getNode("Würzburg"));
    g.dijkstra(g.getNode("Frankfurt"), g.getNode("Stuttgart"));
    g.dijkstra(g.getNode("Frankfurt"), g.getNode("Kassel"));
    g.dijkstra(g.getNode("Frankfurt"), g.getNode("Nürnberg"));
    g.dijkstra(g.getNode("Frankfurt"), g.getNode("Erfurt"));
    g.dijkstra(g.getNode("Frankfurt"), g.getNode("Karlsruhe"));
    g.dijkstra(g.getNode("Frankfurt"), g.getNode("Augsburg"));
    g.dijkstra(g.getNode("Frankfurt"), g.getNode("München"));

    std::cout << "\n***********************************************************************************\n" << std::endl;

    g.dijkstra(g.getNode("Frankfurt"));

    std::cout << "\n***********************************************************************************\n" << std::endl;

    return 0;
}
